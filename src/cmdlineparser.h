#include <getopt.h>
#include <iostream>
#include <string>

int num = -1;
bool is_beep = false;
float sigma = 2.034;
std::string write_file = "default_file.txt";

void PrintHelp(const char* appname)
{
  std::cout <<
    "Usage : \n"
    << appname << " -i influxUrl -o org -b bucket -t token\n"
    "or\n"
    << appname << " -c settings.conf\n"
    "-i/--influx-url :\tURL of your influxdb instance\n"
    "-o/--org : \torg name in influxDb\n"
    "-b/--bucket Bucket to use\n"
    "-t/--token Influx API token, make sure it has write permissions\n"
    "--help:\tShow help\n";
  exit(1);
}

void ProcessArgs(
    int argc, char** argv, 
    std::string& influxUrl,
    std::string& org,
    std::string& bucket,
    std::string& token,
    std::string& configfile
    )
{
  const char* const short_opts = "i:o:b:t:c:h";
  const option long_opts[] = {
    {"influx-url", required_argument, nullptr, 'i'},
    {"org", required_argument, nullptr, 'o'},
    {"bucket", no_argument, nullptr, 'b'},
    {"token", required_argument, nullptr, 't'},
    {"config", required_argument, nullptr, 'c'},
    {"help", no_argument, nullptr, 'h'},
    {nullptr, no_argument, nullptr, 0}
  };

  while (true)
  {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);
    constexpr auto fullarg_length = 4*2+1; // arg[0] + 2x args
    constexpr auto configarg_length = 1*2+1; // arg[0] + config file name

    if (argc<2)
      PrintHelp(argv[0]);

    if (-1 == opt)
      break;
    switch (opt)
    {
      case 'i':
        if (argc!= fullarg_length) {
          std::cout << "Error in arg number\n";
          PrintHelp(argv[0]);
        }
        influxUrl.assign(optarg);
        break;
      case 'o':
        if (argc!= fullarg_length) {
          std::cout << "Error in arg number\n";
          PrintHelp(argv[0]);
        }
        org.assign(optarg);
        break;
      case 'b':
        if (argc!= fullarg_length) {
          std::cout << "Error in arg number\n";
          PrintHelp(argv[0]);
        }
        bucket.assign(optarg);
        break;
      case 't':
        if (argc!= fullarg_length) {
          std::cout << "Error in arg number\n";
          PrintHelp(argv[0]);
        }
        token.assign(optarg);
        break;
      case 'c':
        if (argc!= configarg_length) {
          std::cout << "Error in arg number\n";
          PrintHelp(argv[0]);
        }
        configfile.assign(optarg);
        std::cout << "Not yet implemented";
        exit(2);
        break;

      case 'h': // -h or --help
      case '?': // Unrecognized option
      default:
        PrintHelp(argv[0]);
        break;
    }
  }
}

/*
   $ g++ -std=c++11 opt.cpp 

   $ ./a.out --help
   --num <n>:           Set number of program
   --beep:              Beep the user
   --sigma <val>:       Set sigma of program
   --writeFile <fname>: File to write to
   --help:              Show help

   $ ./a.out --num 10
   Num set to: 10

   $ ./a.out --num 10 --writeFile haha.txt --sigma 3.45 --beep
   Num set to: 10
   Write file set to: haha.txt
   Sigma set to: 3.45
   Beep is set to true
   */
