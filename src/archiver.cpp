/*
Archiver - Crypto prices archive program
Copyright (C) Nolaan <nolaan.gestionsite@gmail.com>
https://nolaan-portfolio.com
This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place, Suite 330, Boston, MA 02111-1307 USA
*/
#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "httplib.h"
#include <cstring>
#include "simdjson.h"
#include "cmdlineparser.h"

#include <iostream>
#include <chrono>
#include <thread>
#include <ctime>
#include <future>
#include <array>

std::string influxUrl {};
std::string org{};
std::string bucket{};
std::string token;
std::string configfile;

// Set Auth header
httplib::Headers headers = {
   {"Content-Type", "text/plain; charset=utf-8"}, {"Accept", "application/json"}
};

struct Exchange { 
  std::string name;
  std::string url;
  std::string endpoint;
  std::string bucketName;
  std::string symbolKey;
  std::string priceKey;
  std::string resultPath;
};

const auto dbDump = [] (std::string& org, std::string& bucket, simdjson::ondemand::array& arr, const Exchange& exchange) {
    auto p = std::chrono::system_clock::now();
    auto now = std::chrono::duration_cast<std::chrono::nanoseconds>(p.time_since_epoch()).count();
    // Setup db client
    httplib::Client influxdbCli(influxUrl);

    std::cout << "Dumping data for " << exchange.name << "\n";
    std::ostringstream lineProtocol;
    for (auto e : arr) {
      std::string_view price;
      if(e[exchange.priceKey].type() == simdjson::ondemand::json_type::number)
      {
        price = std::to_string(e[exchange.priceKey].get_double());
      }
      else if(e[exchange.priceKey].type() == simdjson::ondemand::json_type::string)
      {
        price = std::string_view(e[exchange.priceKey]);
      }
      else
      {
        // std::cout << "Found parsing difficulities with : " << std::string_view(e[exchange.symbolKey]) << "\n";
        price = "0";
      }
      lineProtocol << exchange.name
                   << ",pair="
                   << std::string_view(e[exchange.symbolKey])
                   << " price="
                   << price
                   << " " <<now << "\n";
    }
    std::string influxEndpoint = "/api/v2/write?org=" + org + "&bucket=" + bucket;
    auto res = influxdbCli.Post(influxEndpoint,headers,lineProtocol.str(),"text/plain");
    lineProtocol.str("");
    std::cout << res->body << "\n";
};

const auto dlAndSaveData = [](const Exchange& exchange) {
  // Setup Exchange client
  httplib::Client exchangeCli(exchange.url);
  exchangeCli.enable_server_certificate_verification(false);
  std::cout << "dl Started for " << exchange.name << "\n"; 
  for (int tour = 0;; ++tour) {
		if (auto res = exchangeCli.Get(exchange.endpoint)) {
			if (res->status == 200) {
				/* std::cout << res->body << std::endl; */
				simdjson::ondemand::parser prsr;
				auto message = simdjson::padded_string(res->body);
				simdjson::ondemand::document doc = prsr.iterate(message);
				simdjson::ondemand::array arr;
				if (exchange.resultPath.size()) {
					arr = doc.at_pointer(exchange.resultPath).get_array();
				}
				else
				{
					arr = doc.get_array();
				}

				dbDump(org, bucket, arr, exchange);
			}
		} else {
			auto err = res.error();
			std::cout << "HTTP error(" << exchange.name << "): " << httplib::to_string(err) << std::endl;
		}

    std::this_thread::sleep_for(std::chrono::seconds(60));
  }
};

int main(int argc, char **argv)
{
  // Parse cmdline args 
  ProcessArgs(argc, argv, influxUrl, org, bucket, token, configfile);
  headers.insert({ "Authorization", "token " + token });

  // Binance exchange
  std::vector<Exchange> exchangesParameters;
  std::array<std::array<const char*,7>, 6> conf = {{
    // name ,  url , endpoint ,  measurement name, coinnamefield, pricefield, resultpath
    { "binance","https://api1.binance.com", "/api/v3/ticker/price", "Binance", "symbol", "price", ""},
    { "GateIO","https://api.gateio.ws", "/api/v4/spot/tickers", "GateIO" , "currency_pair", "last", ""},
    { "Kucoin","https://api.kucoin.com", "/api/v1/market/allTickers", "Kucoin" , "symbol", "last", "/data/ticker"},
    { "BitStamp","https://www.bitstamp.net", "/api/v2/ticker/", "Bitstamp" , "pair", "last", ""},
    { "Huobi","https://api.huobi.pro", "/market/tickers", "Huobi" , "symbol", "close", "/data"},
    { "Gemini","https://api.gemini.com", "/v1/pricefeed", "Gemini" , "pair", "price", ""},
  }};
  // binance
  for( const auto &a : conf)
  {
    Exchange ex;
    ex.name = std::string(a[0]);
    ex.url = std::string(a[1]);
    ex.endpoint = std::string(a[2]);
    ex.bucketName = std::string(a[3]);
    ex.symbolKey = std::string(a[4]);
    ex.priceKey = std::string(a[5]);
    ex.resultPath = std::string(a[6]);
    exchangesParameters.push_back(ex);
  }
  // download and save exchange data

  std::vector<std::future<void>> fonctions;
	for (const auto& exchange : exchangesParameters) {
    std::cout << "Launched " << exchange.name << " download\n";
		fonctions.push_back(std::async(dlAndSaveData, exchange));
	}

	for(const auto &f: fonctions){
		f.wait();
	}
  return 0;
}
