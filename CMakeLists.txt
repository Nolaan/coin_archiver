cmake_minimum_required(VERSION 3.14.0 FATAL_ERROR)

set(_OPENSSL_MIN_VER "1.1.1")
# Get the CPPHTTPLIB_VERSION value and use it as a version
# This gets the string with the CPPHTTPLIB_VERSION value from the header.
# This is so the maintainer doesn't actually need to update this manually.
# file(STRINGS httplib.h _raw_version_string REGEX "CPPHTTPLIB_VERSION \"([0-9]+\\.[0-9]+\\.[0-9]+)\"")

# Needed since git tags have "v" prefixing them.
# Also used if the fallback to user agent string is being used.
# string(REGEX MATCH "([0-9]+\\.?)+" _httplib_version "${_raw_version_string}")

project(crypto_archiver VERSION 0.2.1 LANGUAGES CXX)

# Require C++17
set(CMAKE_CXX_STANDARD 20)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O2")


set(SOURCE_FILES src/archiver.cpp src/cmdlineparser.h src/httplib.h src/simdjson.cpp src/simdjson.h)

find_package(ZLIB REQUIRED)
find_package(Threads REQUIRED)
find_package(OpenSSL ${_OPENSSL_MIN_VER} COMPONENTS Crypto SSL REQUIRED)
# Add executable target with source files listed in SOURCE_FILES variable
add_executable(crypto_archiver ${SOURCE_FILES})

target_sources(${PROJECT_NAME}
	PUBLIC
  ${SOURCE_FILES}
)

target_link_libraries(${PROJECT_NAME} PUBLIC
		Threads::Threads
		ZLIB::ZLIB
		OpenSSL::SSL
		OpenSSL::Crypto
)
