FROM debian:bookworm-slim as build

LABEL Nolaan <nolaan.gestionsite@gmail.com>

RUN apt update -y
RUN apt install -y libssl-dev zlib1g-dev build-essential cmake make

ADD CMakeLists.txt /project/
ADD src /project/src

ARG BUILD_TYPE=${BUILD_TYPE:-Release}
RUN mkdir /project/build
WORKDIR /project/build
RUN ls /project && pwd
RUN cmake .. && CMAKE_BUILD_TYPE=$BUILD_TYPE make -j8
RUN ldd /project/build/crypto_archiver

FROM debian:bookworm-slim
RUN apt update -y
RUN apt install -y libssl3 zlib1g libstdc++-11-dev
RUN apt-get clean autoclean && apt-get autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}
WORKDIR /home
COPY --from=0 /project/build/crypto_archiver ./
ADD entrypoint.sh ./
RUN chmod +x ./entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]
